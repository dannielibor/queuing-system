@extends('layouts.main')

@section('content')

<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-content">
            <div class="row">
                <div class="col-lg-6">
                    <!--begin::Portlet-->
                    <div class="m-portlet">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <span class="m-portlet__head-icon m--hide">
                                        <i class="la la-gear"></i>
                                    </span>
                                    <h3 class="m-portlet__head-text">
                                        Now Serving
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <!--begin::Form-->
                        <div class="m-form">
                            <div class="m-portlet__body">
                                <div class="m-form__section m-form__section--first">
                                    <div class="form-group m-form__group">
                                        <center>
                                        <h1 style = "font-size:100px">#{{ $queue[0]->number }}</h1>
                                        </center>
                                    </div>
                                </div>
                            </div>
                            <div class="m-portlet__foot m-portlet__foot--fit"> 
                                <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                                    <div class="m-demo__preview m-demo__preview--btn">
                                        <center>
                                            <form action="/next" method = "get">
                                                <button type="submit" class="btn btn-success btn-lg m-btn m-btn--custom">
                                                    Next
                                                </button>
                                            </form>
                                            <form action="/hold" method = "get">
                                                <button type="submit" class="btn btn-warning btn-lg m-btn m-btn--custom">
                                                    Hold
                                                </button>
                                            </form>
                                            <form action="/reset" method = "get">
                                                <button type="submit" class="btn btn-danger btn-lg m-btn m-btn--custom">
                                                    Reset
                                                </button>
                                            </form>
                                        </center>
                                    </div>
                                </div> 
                            </div>
                        </div>
                        <!--end::Form-->
                    </div>
                    <!--end::Portlet-->
                </div>
                <div class="col-lg-6">
                    <!--begin::Portlet-->
                    <div class="m-portlet">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <span class="m-portlet__head-icon m--hide">
                                        <i class="la la-gear"></i>
                                    </span>
                                    <h3 class="m-portlet__head-text">
                                        Hold
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body">
								<!--begin: Search Form -->
								<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
									<div class="row align-items-center">
										<div class="col-xl-8 order-2 order-xl-1">
											<div class="form-group m-form__group row align-items-center">
												<div class="col-md-4">
													<div class="m-input-icon m-input-icon--left">
														<input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
														<span class="m-input-icon__icon m-input-icon__icon--left">
															<span>
																<i class="la la-search"></i>
															</span>
														</span>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!--end: Search Form -->
		                        <!--begin: Datatable -->
								<table class="m-datatable" id="html_table" width="100%" style = "text-align:center">
									<thead>
										<tr>
											<th title="Field #1">
												Queue #
											</th>
											<th title="Field #2">
												Created at
											</th>
											<th title="Field #3">
												Held at
											</th>
											<th title="Field #4">
												Action
											</th>
										</tr>
									</thead>
									<tbody>
                                        @if(count($pendings) > 0)
                                            @foreach ($pendings as $pending)
                                                <tr>
                                                    <td>
                                                        {{ $pending->number }}
                                                    </td>
                                                    <td>
                                                        blank
                                                    </td>
                                                    <td>
                                                        {{ $pending->created_at->format('M d, Y - g:i A') }}
                                                    </td>
                                                    <td>
                                                        <button type = "submit" class="btn btn-accent">Serve</button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
								</table>
								<!--end: Datatable -->
							</div>
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
