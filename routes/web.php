<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'StudentController@index');
Route::get('/fetch', 'StudentController@fetch');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/admin', function () {
    return redirect('/admin/dashboard');
});
Route::get('/admin/login', function () {
    return redirect('/admin/dashboard');
});
Route::group(['middleware' => 'admin'], function(){

    Route::get('/admin/dashboard', 'AdminController@index');
    Route::get('/next', 'AdminController@next');
    Route::get('/reset', 'AdminController@reset');
    Route::get('/hold', 'AdminController@hold');

});