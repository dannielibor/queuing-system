<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Queue;
use App\Hold;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $queue = Queue::all();
        $pending = Hold::all();
        return view('admin')->with('queue', $queue)->with('pendings', $pending);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function next(){
        $queue = Queue::all();
        $queue[0]->number = $queue[0]->number + 1;
        $queue[0]->save();
        return back();
    }

    public function reset(){
        $queue = Queue::all();
        $queue[0]->number = 0;
        $queue[0]->save();
        $hold = Hold::all();
        foreach($hold as $del){
            $del->delete();
        }
        return back();
    }

    public function hold(){
        $queue = Queue::all();
        $hold = new Hold;
        $hold->number = $queue[0]->number;
        $hold->save();
        $queue[0]->number = $queue[0]->number + 1;
        $queue[0]->save();
        return back();
    }

}
